﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Data;

namespace BMKG.Infrastructure.Interface
{
    public class ConnectionFactory : IConnectionFactory
    {
        private readonly string connectionString;
        public ConnectionFactory(IConfiguration container)
        {
            connectionString = container.GetConnectionString("PostgresConnection");
        }

        public IDbConnection GetConnection
        {
            get
            {
                // konfigurasi postgresql
                var conn = new NpgsqlConnection(connectionString);
                conn.Open();
                return conn;
            }
        }
        #region IDisposable Support
        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);

        }
        #endregion

    }
}
