﻿using System;
using System.Data;

namespace BMKG.Infrastructure.Interface
{
    public interface IConnectionFactory : IDisposable
    {
        IDbConnection GetConnection { get; }
    }
}
