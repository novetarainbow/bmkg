﻿using BMKG.DataAccess.Interface;
using BMKG.Infrastructure.Interface;
using BMKG.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using BMKG.DataAccess.DatabaseQuery;

namespace BMKG.DataAccess
{
    public class JabatanDataAccess
    {
        #region private Properties
        private IConnectionFactory connectionFactory;
        #endregion

        public AsetDataAccess(IConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }

        public async Task<int> Add(JabatanModel jabatanModel)
        {
            int resultId = 0;
            using (IDbConnection con = this.connectionFactory.GetConnection)
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@idjabatan", jabatanModel.Idjabatan);
                parameters.Add("@jabatan", jabatanModel.Jabatan);
                parameters.Add("@keterangan", jabatanModel.Keterangan);
                resultId = (int)await con.ExecuteScalarAsync(JabatanDbQuery.InsertJabatan, parameters, commandType: CommandType.Text);
            }
            return resultId;
        }

        public async Task<int> Delete(int Id)
        {
            int rowAffected = 0;
            using (IDbConnection con = this.connectionFactory.GetConnection)
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@idjabatan", Id);
                rowAffected = await con.ExecuteAsync(JabatanDbQuery.DeleteJabatan, parameters, commandType: CommandType.Text);
            }
            return rowAffected;
        }

        public async Task<JabatanModel> Get(int Id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@idjabatan", Id);

            return await SqlMapper.QuerySingleAsync<JabatanModel>(this.connectionFactory.GetConnection, JabatanDbQuery.GetJabatanByID, parameters);

        }

        public async Task<IEnumerable<JabatanModel>> GetAll()
        {
            return await SqlMapper.QueryAsync<JabatanModel>(this.connectionFactory.GetConnection, JabatanDbQuery.GetJabatanDetails);
        }

        public async Task<int> Update(JabatanModel jabatanModel)
        {
            int rowAffected = 0;

            using (IDbConnection con = this.connectionFactory.GetConnection)
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@idjabatan", jabatanModel.Idjabatan);
                parameters.Add("@jabatan", jabatanModel.Jabatan);
                parameters.Add("@keterangan", jabatanModel.Keterangan);
                rowAffected = (int)await con.ExecuteScalarAsync(JabatanDbQuery.InsertJabatan, parameters, commandType: CommandType.Text);
            }
            return rowAffected;
        }
    }
}
