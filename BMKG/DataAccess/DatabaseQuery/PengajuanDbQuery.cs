﻿namespace BMKG.DataAccess.DatabaseQuery
{
    public class PengajuanDbQuery
    {
        #region query postgresql
        public const string GetPengajuanDetails = "SELECT * FROM public.pengajuan ORDER BY idpengajuan ASC";
        public const string GetPengajuanByID = "SELECT * FROM pengajuan where idpengajuan=@idpengajuan";
        public const string InsertPengajuan = "INSERT INTO public.pengajuan(idpengajuan, idaset, namapengajuan, alasan, kategori, status) VALUES (@idpengajuan, @idaset, @namapengajuan, @alasan, @kategori, @status) RETURNING idpengajuan";
        public const string UpdatePengajuan = "UPDATE pengajuan SET idpengajuan = @idpengajuan, idaset =  @idaset, namapengajuan = @namapengajuan, alasan = @alasan, kategori = @kategori, status = @status Where idpengajuan = @idpengajuan";
        #endregion
    }
}
