﻿namespace BMKG.DataAccess.DatabaseQuery
{
    public class JabatanDbQuery
    {
        #region query postgresql
        public const string GetJabatanDetails = "SELECT * FROM public.jabatan ORDER BY idjabatan ASC";
        public const string GetJabatanByID = "SELECT * FROM jabatan where idjabatan=@idjabatan";
        public const string InsertJabatan = "INSERT INTO public.jabatan(idjabatan, jabatan, keterangan) VALUES (@idjabatan, @jabatan, @keterangan) RETURNING idjabatan";
        public const string UpdateJabatan = "UPDATE jabatan SET idjabatan = @idjabatan, jabatan = @jabatan, keterangan = @keterangan Where idjabatan = @idjabatan";
        #endregion
    }
}
