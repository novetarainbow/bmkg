﻿namespace BMKG.DataAccess.DatabaseQuery
{
    public class UnitKerjaDbQuery
    {
        #region query postgresql
        public const string GetUnitKerjaDetails = "SELECT * FROM public.unitkerja ORDER BY idunitkerja ASC";
        public const string GetUnitKerjaByID = "SELECT * FROM unitkerja where idunitkerja=@idunitkerja";
        public const string InsertUnitKerja = "INSERT INTO public.unitkerja(idunitkerja, unitkerja, keterangan, indukunitkerja) VALUES (@idunitkerja, @unitkerja, @keterangan, @indukunitkerja) RETURNING idunitkerja";
        public const string UpdateUnitKerja = "UPDATE unitkerja SET idunitkerja = @idunitkerja, unitkerja = @unitkerja, keterangan = @keterangan, indukunitkerja = @indukunitkerja";
        #endregion
    }
}
