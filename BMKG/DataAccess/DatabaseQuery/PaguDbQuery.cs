﻿namespace BMKG.DataAccess.DatabaseQuery
{
    public class PaguDbQuery
    {
        #region query postgresql
        public const string GetPaguDetails = "SELECT * FROM public.pagu ORDER BY idpagu ASC";
        public const string GetPaguByID = "SELECT * FROM pagu where idpagu=@idpagu";
        public const string InsertPagu = "INSERT INTO public.pagu(idpagu, priode, kelkegiatan, akunkegiatan, uraiankegiatan, nilaipagu) VALUES (@idpagu, @priode, @kelkegiatan, @akunkegiatan, @uraiankegiatan, @nilaipagu) RETURNING idpagu";
        public const string UpdatePagu = "UPDATE pagu SET idpagu = @idpagu, priode = @priode, kelkegiatan = @kelkegiatan, akunkegiatan = @akunkegiatan, uraiankegiatan = @uraiankegiatan, nilaipagu = @nilaipagu Where idpagu = @idpagu";
        #endregion
    }
}
