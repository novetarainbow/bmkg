﻿namespace BMKG.DataAccess.DatabaseQuery
{
    public class AsetDbQuery
    {
        #region query postgresql
        public const string GetAsetDetails = "SELECT * FROM public.aset ORDER BY asetid ASC";
        public const string GetAsetByID = "SELECT * FROM aset where idaset=@idaset";
        public const string InsertAset = "INSERT INTO public.aset(idaset, kelkegiatan, idunitkerja, kodebarang, nup, nama, nopol, namapengguna, thpengadaan, tglstnk, nilai, keterangan, status) VALUES (@idaset, @kelkegiatan, @idunitkerja, @kodebarang, @nup, @nama, @nopol, @namapengguna, @thpengadaan, @tglstnk, @nilai, @keterangan, @status) RETURNING asetid";
        public const string UpdateAset = "UPDATE aset SET idaset = @idaset, kelkegiatan = @kelkegiatan, idunitkerja = @idunitkerja, kodebarang = @kodebarang, nup = @nup, nama = @nama, nopol = @nopol, namapengguna = @namapengguna, thpengadaan = @thpengadaan, tglstnk = @tglstnk, nilai = @nilai, keterangan = @keterangan, status = @status  Where asetid = @asetid";
        public const string DeleteAset = "DELETE FROM public.aset WHERE idaset=@IdAset";
        #endregion
    }
}
