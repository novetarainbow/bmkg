﻿namespace BMKG.DataAccess.DatabaseQuery
{
    public class TransaksiPemeliharaanDbQuery
    {
        #region query postgresql
        public const string GetTransaksiDetails = "SELECT * FROM public.transaksipemeliharaan ORDER BY idtransaksi ASC";
        public const string GetTransaksiByID = "SELECT * FROM transaksipemeliharaan where idtransaksi=@idtransaksi";
        public const string InsertTransaksi = "INSERT INTO public.transaksipemeliharaan(idtransaksi, kelkegiatan, uraian, nokontrak, unitkerja, jenispemeliharaan, biayapemeliharaan, namapenyedia, tglpemeliharaan, durasi, tglselesai) VALUES (@idtransaksi, @kelkegiatan, @uraian, @nokontrak, @unitkerja, @jenispemeliharaan, @biayapemeliharaan, @namapenyedia, @tglpemeliharaan, @durasi, @tglselesai) RETURNING idtransaksi";
        public const string UpdateTransaksi = "UPDATE transaksipemeliharaan SET idtransaksi = @idtransaksi, kelkegiatan = @kelkegiatan,  uraian = @uraian, nokontrak = @nokontrak, unitkerja = @unitkerja, jenispemeliharaan = @jenispemeliharaan, biayapemeliharaan = @biayapemeliharaan, namapenyedia = @namapenyedia, tglpemeliharaan = @tglpemeliharaan, durasi = @durasi, tglselesai = @tglselesai";
        #endregion
    }
}
