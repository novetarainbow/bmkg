﻿namespace BMKG.DataAccess.DatabaseQuery
{
    public class PenggunaDbQuery
    {
        #region query postgresql
        public const string GetPenggunaDetails = "SELECT * FROM public.pengguna ORDER BY idpengguna ASC";
        public const string GetPenggunaByID = "SELECT * FROM pengguna where idpengguna=@idpengguna";
        public const string InsertPengguna = "INSERT INTO public.pengguna(idpengguna, idjabatan, idunitkerja, nama, email) VALUES (@idpengguna, @idjabatan, @idunitkerja, @nama, @email) RETURNING idpengguna";
        public const string UpdatePengguna = "UPDATE pengguna SET idpengguna = @idpengguna, idjabatan = @idjabatan, idunitkerja = @idunitkerja, nama = @nama, email = @email Where idpengguna = @idpengguna;
        #endregion
    }
}
