﻿namespace BMKG.DataAccess.DatabaseQuery
{
    public class PenyediaDbQuery
    {
        #region query postgresql
        public const string GetPenyediaDetails = "SELECT * FROM public.penyedia ORDER BY idpenyedia ASC";
        public const string GetPenyediaByID = "SELECT * FROM penyedia where idpenyedia=@idpenyedia";
        public const string InsertPenyedia = "INSERT INTO public.penyedia(idpenyedia, namapenyedia, alamat, notlp, email, npwp, namabank, namarekening, nomorrekening, keterangan, status) VALUES (@idpenyedia, @namapenyedia, @alamat, @notlp, @email, @npwp, @namabank, @namarekening, @nomorrekening, @keterangan, @status RETURNING idpenyedia ";
        public const string UpdatePenyedia = "UPDATE penyedia SET idpenyedia = @idpenyedia, namapenyedia = @namapenyedia, alamat = @alamat, notlp = @notlp, email = @email, npwp = @npwp, namabank = @namabank, namarekening = @namarekening, nomorrekening = @nomorrekening, keterangan = @keterangan, status = @status";
        #endregion
    }
}
