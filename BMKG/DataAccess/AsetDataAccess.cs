﻿using BMKG.DataAccess.Interface;
using BMKG.Infrastructure.Interface;
using BMKG.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using BMKG.DataAccess.DatabaseQuery;

namespace BMKG.DataAccess
{
    public class AsetDataAccess
    {
        #region private Properties
        private IConnectionFactory connectionFactory;
        #endregion

        public AsetDataAccess(IConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }

        public async Task<int> Add(AsetModel asetModel)
        {
            int resultId = 0;
            using (IDbConnection con = this.connectionFactory.GetConnection)
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@idaset", asetModel.IdAset);
                parameters.Add("@kelkegiatan", asetModel.KelKegiatan);
                parameters.Add("@idunitkerja", asetModel.UnitKerjaModel.IdUnitKerja);
                parameters.Add("@kodebarang", asetModel.KodeBarang);
                parameters.Add("@nup", asetModel.Nup);
                parameters.Add("@nama", asetModel.Nama);
                parameters.Add("@nopol", asetModel.Nopol);
                parameters.Add("@namapengguna", asetModel.NamaPengguna);
                parameters.Add("@thpengadaan", asetModel.ThPengadaan);
                parameters.Add("@tglstnk", asetModel.TglSTNK);
                parameters.Add("@nilai", asetModel.Nilai);
                parameters.Add("@keterangan", asetModel.Keterangan);
                parameters.Add("@status", asetModel.Status);
                resultId = (int)await con.ExecuteScalarAsync(AsetDbQuery.InsertAset, parameters, commandType: CommandType.Text);
            }
            return resultId;
        }

        public async Task<int> Delete(int Id)
        {
            int rowAffected = 0;
            using (IDbConnection con = this.connectionFactory.GetConnection)
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@idaset", Id);
                rowAffected = await con.ExecuteAsync(AsetDbQuery.DeleteAset, parameters, commandType: CommandType.Text);
            }
            return rowAffected;
        }

        public async Task<AsetModel> Get(int Id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@idaset", Id);

            return await SqlMapper.QuerySingleAsync<AsetModel>(this.connectionFactory.GetConnection, AsetDbQuery.GetAsetByID, parameters);

        }

        public async Task<IEnumerable<AsetModel>> GetAll()
        {
            return await SqlMapper.QueryAsync<AsetModel>(this.connectionFactory.GetConnection, AsetDbQuery.GetAsetDetails);
        }

        public async Task<int> Update(AsetModel asetModel)
        {
            int rowAffected = 0;

            using (IDbConnection con = this.connectionFactory.GetConnection)
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@idaset", asetModel.IdAset);
                parameters.Add("@kelkegiatan", asetModel.KelKegiatan);
                parameters.Add("@idunitkerja", asetModel.UnitKerjaModel.IdUnitKerja);
                parameters.Add("@kodebarang", asetModel.KodeBarang);
                parameters.Add("@nup", asetModel.Nup);
                parameters.Add("@nama", asetModel.Nama);
                parameters.Add("@nopol", asetModel.Nopol);
                parameters.Add("@namapengguna", asetModel.NamaPengguna);
                parameters.Add("@thpengadaan", asetModel.ThPengadaan);
                parameters.Add("@tglstnk", asetModel.TglSTNK);
                parameters.Add("@nilai", asetModel.Nilai);
                parameters.Add("@keterangan", asetModel.Keterangan);
                parameters.Add("@status", asetModel.Status);
                rowAffected = (int)await con.ExecuteScalarAsync(AsetDbQuery.UpdateAset, parameters, commandType: CommandType.Text);
            }
            return rowAffected;
        }
    }
}
