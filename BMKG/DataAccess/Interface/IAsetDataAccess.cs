﻿using BMKG.Model;

namespace BMKG.DataAccess.Interface
{
    interface IAsetDataAccess : IGenericDataAcces<AsetModel>
    {
    }
}
