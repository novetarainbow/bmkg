﻿using BMKG.Model;

namespace BMKG.DataAccess.Interface
{
    interface IJabatanDataAccess : IGenericDataAcces<JabatanModel>
    {
    }
}