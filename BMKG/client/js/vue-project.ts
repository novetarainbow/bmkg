import Vue from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VeeValidate from 'vee-validate';
import Hello from './components/Hello.vue';

import AssetLocation from './components/assetManagement/AssetLocation.vue';
import AssetType from './components/assetManagement/AssetType.vue';
import AssetCategory from './components/assetManagement/AssetCategory.vue';

import STNKList from './components/stnk/STNKList.vue';
import Inbox from './components/inbox/Inbox.vue';

import BelanjaBahanNonJasa from './components/transaksiPemeliharaan/BelanjaBahanNonJasa.vue';
import BelanjaJasaOutsourcing from './components/transaksiPemeliharaan/BelanjaJasaOutsourcing.vue';
import HonorJasaOutsourcing from './components/transaksiPemeliharaan/HonorJasaOutsourcing.vue';
import PemeliharaanGedungDanBangunan from './components/transaksiPemeliharaan/PemeliharaanGedungDanBangunan.vue';
import PemeliharaanKendaraanOperasional from './components/transaksiPemeliharaan/PemeliharaanKendaraanOperasional.vue';
import PemeliharaanKompleksGedungSerbagunaCiteko from './components/transaksiPemeliharaan/PemeliharaanKompleksGedungSerbagunaCiteko.vue';
import PemeliharaanPeralatanMesin from './components/transaksiPemeliharaan/PemeliharaanPeralatanMesin.vue';
import Dashboard from './components/Dashboard.vue';
import Pagu from './components/Pagu.vue';
import DaftarAset from './components/database/DaftarAset.vue';
import DaftarPenyedia from './components/database/DaftarPenyedia.vue';
import DaftarPengguna from './components/database/DaftarPengguna.vue';
import Jabatan from './components/database/Jabatan.vue';
import UnitKerja from './components/database/UnitKerja.vue';
import MonitoringSTNK from './components/monitoring/MonitoringSTNK.vue';
import PenghapusanAset from './components/PengapusanAset.vue';

Vue.use(VeeValidate, {
    classes: true
});

Vue.component('fa', FontAwesomeIcon);

// components must be registered BEFORE the app root declaration
Vue.component('hello', Hello);

//Dashboard
Vue.component('dashboard', Dashboard);

//Service
Vue.component('pagu', Pagu);

//Database
Vue.component('daftar-aset', DaftarAset);
Vue.component('daftar-penyedia', DaftarPenyedia);
Vue.component('daftar-pengguna', DaftarPengguna);
Vue.component('jabatan', Jabatan);
Vue.component('unit-kerja', UnitKerja);

//Monitoring
Vue.component('monitoring-stnk', MonitoringSTNK);

//Penghapusan Aset
Vue.component('penghapusan-aset', PenghapusanAset);


//Transaksi Pemeliharaan
Vue.component('belanja-bahan-non-jasa', BelanjaBahanNonJasa);
Vue.component('belanja-jasa-outsourcing', BelanjaJasaOutsourcing);
Vue.component('honor-jasa-outsourcing', HonorJasaOutsourcing);
Vue.component('pemeliharaan-gedung-dan-bangunan', PemeliharaanGedungDanBangunan);
Vue.component('pemeliharaan-kendaraan-operasional', PemeliharaanKendaraanOperasional);
Vue.component('pemeliharaan-kompleks-gedung-serbaguna-citeko', PemeliharaanKompleksGedungSerbagunaCiteko);
Vue.component('pemeliharaan-peralatan-mesin', PemeliharaanPeralatanMesin);


//UNUSED
//Inbox
Vue.component('inbox', Inbox);

//Asset
Vue.component('asset-location', AssetLocation);
Vue.component('asset-type', AssetType);
Vue.component('asset-category', AssetCategory);

//STNK
Vue.component('stnk-list', STNKList);




// bootstrap the Vue app from the root element <div id="app"></div>
new Vue().$mount('#app');

let sidebarCollapse = <HTMLButtonElement>document.getElementById("sidebarCollapse");

if (sidebarCollapse !== null && sidebarCollapse !== undefined) {
    sidebarCollapse.onclick = () => {
        let sidebar = <HTMLElement>document.getElementById("sidebar");

        if (sidebar.className === 'active') {
            sidebar.className = '';
        }
        else {
            sidebar.className = 'active';
        }
    }
}