import { library } from '@fortawesome/fontawesome-svg-core'
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner'
import { faBell } from '@fortawesome/free-solid-svg-icons/faBell'
import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus'
import { faTrash } from '@fortawesome/free-solid-svg-icons/faTrash'

//ImpossibleTable
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons/faEllipsisH'
//InternalChat
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons/faSyncAlt'

//Option
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons/faEllipsisV'

//Navigation bar
import { faChartBar } from '@fortawesome/free-solid-svg-icons/faChartBar'
import { faSearch } from '@fortawesome/free-solid-svg-icons/faSearch'
import { faInbox } from '@fortawesome/free-solid-svg-icons/faInbox'
import { faChartLine } from '@fortawesome/free-solid-svg-icons/faChartLine'
import { faFileAlt } from '@fortawesome/free-solid-svg-icons/faFileAlt'
import { faBook } from '@fortawesome/free-solid-svg-icons/faBook'
import { faCog } from '@fortawesome/free-solid-svg-icons/faCog'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons/faChevronDown'
import { faChevronUp } from '@fortawesome/free-solid-svg-icons/faChevronUp'
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons/faChevronLeft'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons/faChevronRight'

//DatePicker
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons/faCalendarAlt'

//AddQuestionModal
import { faListUl } from '@fortawesome/free-solid-svg-icons/faListUl'
import { faAlignLeft } from '@fortawesome/free-solid-svg-icons/faAlignLeft'
import { faBars } from '@fortawesome/free-solid-svg-icons/faBars'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons/faCheckCircle'
import { faChevronCircleDown } from '@fortawesome/free-solid-svg-icons/faChevronCircleDown'
import { faSlidersH } from '@fortawesome/free-solid-svg-icons/faSlidersH'
import { faStar } from '@fortawesome/free-solid-svg-icons/faStar'
import { faCircle } from '@fortawesome/free-solid-svg-icons/faCircle'
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons/faTrashAlt'

//SearchCustomer
import { faUser } from '@fortawesome/free-solid-svg-icons/faUser'
import { faCar } from '@fortawesome/free-solid-svg-icons/faCar'
import { faIdCard } from '@fortawesome/free-solid-svg-icons/faIdCard'
import { faIdBadge } from '@fortawesome/free-solid-svg-icons/faidBadge'
import { faMap } from '@fortawesome/free-solid-svg-icons/faMap'
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons/faExclamationCircle'

//Sort
import { faSort } from '@fortawesome/free-solid-svg-icons/faSort'

//Restore
import { faWindowRestore } from '@fortawesome/free-solid-svg-icons/faWindowRestore'

import { faEye } from '@fortawesome/free-solid-svg-icons/faEye'
import { faThumbtack } from '@fortawesome/free-solid-svg-icons/faThumbtack'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope'
import { faUpload } from '@fortawesome/free-solid-svg-icons/faUpload'
import { faDownload } from '@fortawesome/free-solid-svg-icons/faDownload'
import { faEdit } from '@fortawesome/free-solid-svg-icons/faEdit'

import { faMoneyCheckAlt } from '@fortawesome/free-solid-svg-icons/faMoneyCheckAlt'
import { faCashRegister } from '@fortawesome/free-solid-svg-icons/faCashRegister'
import { faBuilding } from '@fortawesome/free-solid-svg-icons/faBuilding'

import { faDatabase } from '@fortawesome/free-solid-svg-icons/faDatabase'
import { faTachometerAlt } from '@fortawesome/free-solid-svg-icons/faTachometerAlt'

library.add(faBuilding);
library.add(faCashRegister);
library.add(faMoneyCheckAlt);
library.add(faTrashAlt);
library.add(faSpinner);
library.add(faPlus);
library.add(faTrash);
library.add(faBell);
library.add(faChartBar);
library.add(faSearch);
library.add(faInbox);
library.add(faChartLine);
library.add(faFileAlt);
library.add(faBook);
library.add(faCog);
library.add(faEllipsisH);
library.add(faCalendarAlt);
library.add(faChevronDown);
library.add(faChevronUp);
library.add(faChevronLeft);
library.add(faChevronRight);

library.add(faListUl);
library.add(faAlignLeft);
library.add(faBars);
library.add(faCheckCircle);
library.add(faChevronCircleDown);
library.add(faSlidersH);
library.add(faStar);
library.add(faCircle);
library.add(faCheck);
library.add(faSyncAlt);
library.add(faEllipsisV);

library.add(faUser);
library.add(faCar);
library.add(faIdCard);
library.add(faIdBadge);
library.add(faMap);
library.add(faExclamationCircle);
library.add(faSort);

library.add(faWindowRestore);

library.add(faEye);
library.add(faThumbtack);
library.add(faEnvelope);
library.add(faUpload);
library.add(faDownload);
library.add(faEdit);
library.add(faDatabase);
library.add(faTachometerAlt);

