﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMKG.Model
{
    public class PengajuanModel
    {
        #region
        private int idPengajuan;
        private AsetModel asetModel;
        private int idAset;
        private string namaPengajuan;
        private string alasan;
        private string kategori;
        private string status;
        private string file;
        #endregion

        #region
        public int IdPengajuan { get => idPengajuan; set => idPengajuan = value; }
        public string NamaPengajuan { get => namaPengajuan; set => namaPengajuan = value; }
        public string Alasan { get => alasan; set => alasan = value; }
        public string Kategori { get => kategori; set => kategori = value; }
        public string Status { get => status; set => status = value; }
        public string File { get => file; set => file = value; }
        public AsetModel AsetModel { get => asetModel; set => asetModel = value; }
        public int IdAset { get => idAset; set => idAset = value; }

        #endregion







    }
}
