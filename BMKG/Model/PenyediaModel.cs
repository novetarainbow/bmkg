﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMKG.Model
{
    public class PenyediaModel
    {
        #region
        private int idPenyedia;
        private string namaPenyedia;
        private string alamat;
        private double noTlp;
        private string email;
        private string npwp;
        private string namaBank;
        private string namaRekening;
        private string nomorRekening;
        private string keterangan;
        private string status;
        #endregion

        #region
        public int IdPenyedia { get => idPenyedia; set => idPenyedia = value; }
        public string NamaPenyedia { get => namaPenyedia; set => namaPenyedia = value; }
        public string Alamat { get => alamat; set => alamat = value; }
        public double NoTlp { get => noTlp; set => noTlp = value; }
        public string Email { get => email; set => email = value; }
        public string Npwp { get => npwp; set => npwp = value; }
        public string NamaBank { get => namaBank; set => namaBank = value; }
        public string NamaRekening { get => namaRekening; set => namaRekening = value; }
        public string NomorRekening { get => nomorRekening; set => nomorRekening = value; }
        public string Keterangan { get => keterangan; set => keterangan = value; }
        public string Status { get => status; set => status = value; }
        #endregion

    }
}
