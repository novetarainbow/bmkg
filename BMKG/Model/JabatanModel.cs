﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMKG.Model
{
    public class JabatanModel
    {
        #region
        private int idJabatan;
        private string jabatan;
        private string keterangan;
        #endregion

        #region
        public int IdJabatan { get => idJabatan; set => idJabatan = value; }
        public string Jabatan { get => jabatan; set => jabatan = value; }
        public string Keterangan { get => keterangan; set => keterangan = value; }

        #endregion



    }
}
