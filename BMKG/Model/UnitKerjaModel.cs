﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMKG.Model
{
    public class UnitKerjaModel
    {
        #region
        private int idUnitKerja;
        private string unitKerja;
        private string keterangan;
        private string indukUnitKerja;
        #endregion

        #region
        public int IdUnitKerja { get => idUnitKerja; set => idUnitKerja = value; }
        public string UnitKerja { get => unitKerja; set => unitKerja = value; }
        public string Keterangan { get => keterangan; set => keterangan = value; }
        public string IndukUnitKerja { get => indukUnitKerja; set => indukUnitKerja = value; }
        #endregion




    }
}
