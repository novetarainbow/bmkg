﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMKG.Model
{
    public class PenggunaModel
    {
        #region
        private int idPengguna;
        private JabatanModel jabatanModel;
        private int idJabatan;
        private UnitKerjaModel unitKerjaModel;
        private int idUnitKerja;
        private string nama;
        private string email;
        private string password;
        #endregion

        #region
        public int IdPengguna { get => idPengguna; set => idPengguna = value; }
        public string Nama { get => nama; set => nama = value; }
        public string Email { get => email; set => email = value; }
        public string Password { get => password; set => password = value; }
        public JabatanModel JabatanModel { get => jabatanModel; set => jabatanModel = value; }
        public int IdJabatan { get => idJabatan; set => idJabatan = value; }
        public UnitKerjaModel UnitKerjaModel { get => unitKerjaModel; set => unitKerjaModel = value; }
        public int IdUnitKerja { get => idUnitKerja; set => idUnitKerja = value; }
        #endregion
    }
}
