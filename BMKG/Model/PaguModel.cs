﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMKG.Model
{
    public class PaguModel
    {
        #region
        private int idPagu;
        private string priode;
        private string kelKegiatan;
        private string akunKegiatan;
        private string uraianKegiatan;
        private string nilaiPagu;
        #endregion

        #region
        public int IdPagu { get => idPagu; set => idPagu = value; }
        public string Priode { get => priode; set => priode = value; }
        public string KelKegiatan { get => kelKegiatan; set => kelKegiatan = value; }
        public string AkunKegiatan { get => akunKegiatan; set => akunKegiatan = value; }
        public string UraianKegiatan { get => uraianKegiatan; set => uraianKegiatan = value; }
        public string NilaiPagu { get => nilaiPagu; set => nilaiPagu = value; }
        #endregion
    }
}
