﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BMKG.Model
{
    public class AsetModel
    {
        #region
        private int idAset;
        private string kelKegiatan;
        private UnitKerjaModel unitKerjaModel;
        private int idUnitKerja;
        private string kodeBarang;
        private string nup;
        private string nama;
        private string nopol;
        private string namaPengguna;
        private DateTime thPengadaan;
        private DateTime tglSTNK;
        private string nilai;
        private string keterangan;
        private string status;
        private string file;
        #endregion

        #region
        public int IdAset { get => idAset; set => idAset = value; }
        public string KelKegiatan { get => kelKegiatan; set => kelKegiatan = value; }
        public string KodeBarang { get => kodeBarang; set => kodeBarang = value; }
        public string Nup { get => nup; set => nup = value; }
        public string Nama { get => nama; set => nama = value; }
        public string Nopol { get => nopol; set => nopol = value; }
        public string NamaPengguna { get => namaPengguna; set => namaPengguna = value; }
        public DateTime ThPengadaan { get => thPengadaan; set => thPengadaan = value; }
        public DateTime TglSTNK { get => tglSTNK; set => tglSTNK = value; }
        public string Nilai { get => nilai; set => nilai = value; }
        public string Keterangan { get => keterangan; set => keterangan = value; }
        public string Status { get => status; set => status = value; }
        public string File { get => file; set => file = value; }
        public UnitKerjaModel UnitKerjaModel { get => unitKerjaModel; set => unitKerjaModel = value; }
        public int IdUnitKerja { get => idUnitKerja; set => idUnitKerja = value; }

        #endregion
    }
}
